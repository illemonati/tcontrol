package tcontrol

import (
	"github.com/google/uuid"
	"runtime/debug"
)

type ClientInfo struct {
	Version  string
	HostInfo HostInfo
	ClientID uuid.UUID
}

func NewClientInfo() *ClientInfo {
	clientInfo := new(ClientInfo)
	if buildInfo, ok := debug.ReadBuildInfo(); ok {
		clientInfo.Version = buildInfo.Main.Version
	}
	clientInfo.HostInfo = *NewHostInfo()
	var err error
	clientInfo.ClientID, err = uuid.NewRandom()
	if err != nil {
		return nil
	}
	return clientInfo
}
