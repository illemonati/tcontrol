package tcontrol

import (
	"os"
	"runtime"
)

type HostInfo struct {
	HostName string
	OS       string
	Arch     string
	NumCPU   int
}

func NewHostInfo() *HostInfo {
	hInfo := new(HostInfo)
	hInfo.OS = runtime.GOOS
	hInfo.Arch = runtime.GOARCH
	hInfo.NumCPU = runtime.NumCPU()
	if hostname, err := os.Hostname(); err == nil {
		hInfo.HostName = hostname
	}
	return hInfo
}
