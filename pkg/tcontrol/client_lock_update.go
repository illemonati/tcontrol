package tcontrol

const (
	ClientLockUpdateEvent ClientEventType = "lock-update"
)

type ClientLockedState string

const (
	Locked           ClientLockedState = "locked"
	AttemptingUnlock ClientLockedState = "attempting-unlock"
	Unlocked         ClientLockedState = "unlocked"
	Unknown          ClientLockedState = "unknown"
)
