package tcontrol

import (
	"github.com/google/uuid"
	"time"
)

type ClientEventType string

type ClientEvent struct {
	EventType    ClientEventType
	EventMessage any
	TimeStamp    time.Time
	ClientID     uuid.UUID
}
