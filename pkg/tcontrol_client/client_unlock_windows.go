//go:build windows

package tcontrol_client

import (
	"TControl/pkg/tcontrol"
	"syscall"
	"unsafe"
)

var user32 = syscall.NewLazyDLL("user32.dll")
var getForegroundWindow = user32.NewProc("GetForegroundWindow")
var getWindowTextW = user32.NewProc("GetWindowTextW")
var getWindowTextLengthW = user32.NewProc("GetWindowTextLengthW")

func GetClientLockedState() tcontrol.ClientLockedState {
	foregroundWindow, _, _ := getForegroundWindow.Call()
	if foregroundWindow == 0 {
		return tcontrol.Unknown
	}
	foregroundWindowTextLength, _, _ := getWindowTextLengthW.Call(uintptr(foregroundWindow))
	foregroundWindowTextBuf := make([]uint16, int(foregroundWindowTextLength)+1)
	readLen, _, _ := getWindowTextW.Call(uintptr(foregroundWindow), uintptr(unsafe.Pointer(&foregroundWindowTextBuf[0])), uintptr(foregroundWindowTextLength+1))
	foregroundWindowText := syscall.UTF16ToString(foregroundWindowTextBuf[:readLen])
	if foregroundWindowText == "Windows Default Lock Screen" {
		return tcontrol.Locked
	} else if foregroundWindowText == "UnlockingWindow" {
		return tcontrol.AttemptingUnlock
	}
	return tcontrol.Unlocked
}
