package tcontrol_client

import (
	"TControl/pkg/tcontrol"
	"bytes"
	"encoding/json"
	"net/http"
)

func (client *Client) SendClientEvent(event tcontrol.ClientEvent) (err error) {
	event.ClientID = client.Info.ClientID
	postBody, _ := json.Marshal(event)
	clientEventPath := client.ServerURL.JoinPath("/client-event")
	_, err = http.Post(clientEventPath.String(), "application/json", bytes.NewBuffer(postBody))
	return
}
