package tcontrol_client

import (
	"TControl/pkg/tcontrol"
	"log"
	"time"
)

func (client *Client) StartUnlockDetection() {
	clientLockedState := tcontrol.Unknown
	for {
		select {
		case <-client.Shutdown:
			break
		default:
			newClientLockedState := GetClientLockedState()
			if newClientLockedState == clientLockedState || newClientLockedState == tcontrol.Unknown {
				continue
			} else {
				clientLockedState = newClientLockedState
			}
			event := tcontrol.ClientEvent{
				EventType:    tcontrol.ClientLockUpdateEvent,
				EventMessage: clientLockedState,
				TimeStamp:    time.Now(),
				ClientID:     client.Info.ClientID,
			}
			err := client.SendClientEvent(event)
			if err != nil {
				log.Printf("Client Lock Update Error: %v", err)
			}
			time.Sleep(time.Second * 1)
		}
	}
}
