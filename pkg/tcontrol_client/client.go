package tcontrol_client

import (
	"TControl/pkg/tcontrol"
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"time"
)

const HeartBeatInterval time.Duration = time.Second * 5

type Client struct {
	Info      tcontrol.ClientInfo
	ServerURL url.URL
	Shutdown  chan bool
}

func NewClient(serverURL url.URL) *Client {
	client := new(Client)
	client.Info = *tcontrol.NewClientInfo()
	client.ServerURL = serverURL
	client.Shutdown = make(chan bool)
	return client
}

func (client *Client) Start() {
	go client.StartHeartBeats()
	go client.StartUnlockDetection()
}

func (client *Client) StartHeartBeats() {
	for {
		select {
		case <-client.Shutdown:
			break
		default:
			client.HeartBeat()
			time.Sleep(HeartBeatInterval)
		}
	}
}

func (client *Client) HeartBeat() {
	postBody, _ := json.Marshal(client.Info)
	heartBeatPath := client.ServerURL.JoinPath("/heartbeat")
	_, err := http.Post(heartBeatPath.String(), "application/json", bytes.NewBuffer(postBody))
	if err != nil {
		log.Printf("HeartBeat error: %v", err)
	}

}
