package tcontrol_server

import (
	"TControl/pkg/tcontrol"
	"embed"
	jsoniter "github.com/json-iterator/go"
	"github.com/op/go-logging"
	"io/fs"
	"net/http"
	"sync"
	"time"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
var log = logging.MustGetLogger("tserver")

//go:embed web/tcontrol-server-web-interface/build
var web embed.FS

type ClientConnectionKey string

type Server struct {
	ClientConnections map[ClientConnectionKey]*ClientConnection
	HTTPMux           *http.ServeMux
	API               *ServerAPI
	ListenAddress     string
	mutex             *sync.RWMutex
}

type ClientConnection struct {
	ClientConnectionKey
	tcontrol.ClientInfo
	LastHeartBeat time.Time
	LockedState   tcontrol.ClientLockedState
}

func NewServer(listenAddress string) *Server {
	server := new(Server)
	server.ClientConnections = map[ClientConnectionKey]*ClientConnection{}
	server.HTTPMux = http.NewServeMux()
	server.ListenAddress = listenAddress
	server.mutex = new(sync.RWMutex)

	server.SetupAPI()

	server.HandleHeartBeat()
	server.HandleClientEvent()
	server.HandleWebInterface()

	return server
}

func (server *Server) HandleHeartBeat() {
	server.HTTPMux.HandleFunc("/heartbeat", func(res http.ResponseWriter, req *http.Request) {
		var clientInfo tcontrol.ClientInfo
		if err := json.NewDecoder(req.Body).Decode(&clientInfo); err != nil {
			return
		}
		clientConnectionKey := ClientConnectionKey(clientInfo.ClientID.String())

		server.mutex.Lock()
		if connection, ok := server.ClientConnections[clientConnectionKey]; !ok {
			clientConnection := ClientConnection{
				clientConnectionKey, clientInfo, time.Now(), tcontrol.Unknown,
			}
			server.ClientConnections[clientConnectionKey] = &clientConnection
			log.Infof("New connection from %+v", clientConnection)
		} else {
			connection.LastHeartBeat = time.Now()
			log.Debugf("Hearbeat from %+v", connection)
		}

		server.mutex.Unlock()

		res.WriteHeader(200)
	})
}

func (server *Server) HandleWebInterface() {
	build, err := fs.Sub(web, "web/tcontrol-server-web-interface/build")
	if err != nil {
		log.Warningf("Unable to start Web Interface: %v", err)
		return
	}
	server.HTTPMux.Handle("/", http.FileServer(http.FS(build)))
}

func (server *Server) Start() {
	err := http.ListenAndServe(server.ListenAddress, server.HTTPMux)
	log.Critical(err)
}
