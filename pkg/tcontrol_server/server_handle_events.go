package tcontrol_server

import (
	"TControl/pkg/tcontrol"
	"net/http"
)

func (server *Server) HandleClientEvent() {
	server.HTTPMux.HandleFunc("/client-event", func(res http.ResponseWriter, req *http.Request) {
		var clientEvent tcontrol.ClientEvent
		if err := json.NewDecoder(req.Body).Decode(&clientEvent); err != nil {
			return
		}
		clientConnectionKey := ClientConnectionKey(clientEvent.ClientID.String())

		switch clientEvent.EventType {
		case tcontrol.ClientLockUpdateEvent:
			server.HandleClientLockUpdate(clientConnectionKey, clientEvent.EventMessage)
			res.WriteHeader(200)
			break
		default:
			res.WriteHeader(400)
		}
	})
}

func (server *Server) HandleClientLockUpdate(clientConnectionKey ClientConnectionKey, eventMessage any) {
	server.mutex.Lock()
	if connection, ok := server.ClientConnections[clientConnectionKey]; ok {
		connection.LockedState = tcontrol.ClientLockedState(eventMessage.(string))
		log.Infof("connection: %+v change state: %s", clientConnectionKey, connection.LockedState)
	}

	server.mutex.Unlock()

}
