package tcontrol_server

import (
	"net/http"
)

type ServerAPI struct {
	HTTPMux *http.ServeMux
	Server  *Server
}

func (server *Server) SetupAPI() {
	server.API = new(ServerAPI)
	server.API.HTTPMux = http.NewServeMux()
	server.API.Server = server

	server.API.HandleListClients()

	server.HTTPMux.Handle("/api/", http.StripPrefix("/api", server.API.HTTPMux))
}

func (api *ServerAPI) HandleListClients() {
	api.HTTPMux.HandleFunc("/clients", func(res http.ResponseWriter, req *http.Request) {

		api.Server.mutex.RLock()

		clientConnectionsJSON, err := json.Marshal(api.Server.ClientConnections)

		api.Server.mutex.RUnlock()

		if err != nil {
			res.Header().Add("Content-Type", "application/json")
			res.WriteHeader(200)
			_, err := res.Write([]byte(err.Error()))
			if err != nil {
				log.Warningf("Unable to write error: %v", err)
			}
			return
		}

		res.Header().Add("Content-Type", "application/json")
		res.WriteHeader(200)
		_, err = res.Write(clientConnectionsJSON)
		if err != nil {
			log.Warningf("Unable to write clients list: %v", err)
		}
	})
}
