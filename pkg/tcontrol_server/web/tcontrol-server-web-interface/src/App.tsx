import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';


export default function App() {
    return (
        <Container maxWidth="md">
            <Typography variant="h4">Hi</Typography>
        </Container>
    );
}