package main

import (
	"TControl/pkg/tcontrol_client"
	_ "embed"
	"log"
	"net/url"
)

//go:embed server-url.txt
var serverURLString string

func main() {
	log.Printf("Starting TControl Cient")
	serverURL, err := url.Parse(serverURLString)
	if err != nil {
		log.Printf("Bad Server URL: %v", err)
	}
	log.Printf("Server URL: %s", serverURL)
	client := tcontrol_client.NewClient(*serverURL)
	log.Printf("Client Info: %+v", client.Info)

	client.Start()

	log.Printf("TControl Client Started")

	for range client.Shutdown {
		log.Println(1)
	}
}
