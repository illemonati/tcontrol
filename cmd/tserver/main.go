package main

import (
	"TControl/pkg/tcontrol_server"
	_ "embed"
	"github.com/op/go-logging"
	"os"
)

//go:embed listen-address.txt
var listenAddress string

var logFormat = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
)
var log = logging.MustGetLogger("tserver")

func main() {

	consoleLogBackend := logging.NewLogBackend(os.Stderr, "", 0)

	consoleLogBackendLeveled := logging.AddModuleLevel(consoleLogBackend)
	consoleLogBackendLeveled.SetLevel(logging.INFO, "")

	consoleLogBackendFormatter := logging.NewBackendFormatter(consoleLogBackendLeveled, logFormat)

	f, err := os.OpenFile("tserver.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	defer f.Close()

	fileLogBackend := logging.NewLogBackend(f, "", 0)
	fileLogBackendFormatter := logging.NewBackendFormatter(fileLogBackend, logFormat)

	logging.SetBackend(consoleLogBackendFormatter, fileLogBackendFormatter)

	log.Noticef("Starting TControl Server on %s", listenAddress)
	server := tcontrol_server.NewServer(listenAddress)
	server.Start()
	log.Noticef("TControl Server Started")
}
