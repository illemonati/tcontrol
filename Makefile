CLIENT_NAME=tclient
SERVER_NAME=tserver

CLIENT_ARCHS = amd64 386
CLIENT_OSS = windows

SERVER_ARCHS = amd64 arm64
SERVER_OSS = windows darwin linux


.DEFAULT_GOAL := all


client:
	$(foreach OS,$(CLIENT_OSS),$(foreach ARCH,$(CLIENT_ARCHS),\
		GOARCH=${ARCH} GOOS=${OS} go build -ldflags="-s -w" -ldflags -H=windowsgui -o ./build/client/${CLIENT_NAME}-${OS}-${ARCH}$(if $(filter windows,${OS}),.exe) ./cmd/tclient;\
	))

server-web:
	cd ./pkg/tcontrol_server/web/tcontrol-server-web-interface; \
		yarn; \
    	yarn build

server-no-web:
	$(foreach OS,$(SERVER_OSS),$(foreach ARCH,$(SERVER_ARCHS),\
		GOARCH=${ARCH} GOOS=${OS} go build -o ./build/server/${SERVER_NAME}-${OS}-${ARCH}$(if $(filter windows,${OS}),.exe) ./cmd/tserver;\
	))

server: server-web server-no-web

all: client server


clean:
	go clean
	rm -r ./build/